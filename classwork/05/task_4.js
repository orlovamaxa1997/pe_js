/*
Створити об'єкт danItStudent,
У якого такі властивості: ім'я, прізвище, кількість зданих домашніх работ.
Вивести об'єкт у консоль.
Після чого спочатку запитати у користувача "Яку властивість ви хочете змінити?",
Потім - "На яке значення?". Змінити потрібну властивість і вивести об'єкт у консоль.
*/

const danItStudent = {
    firstName: "Mariia",
    lastName: "Koval",
    numberOfSubmittedHomewors: "6"
}

console.log(danItStudent);

let askProperty = prompt("Яку властивість ви хочете змінити?");
let askValue = prompt("На яке значення?");

danItStudent[askProperty] = askValue;

console.log(danItStudent);
