/*
Запитати в користувача ім'я, прізвище, вік (перевірити на число
і що число між 15 і 100), чи є діти, якого кольору очі.
Усі ці дані записати в об'єкт.
Перебрати об'єкт і вивести у консолі фразу
`Властивість ${name}: ${значення name}` і так з усіма властивостями
*/

let persone = {};

let firstName = prompt("What is your first name?");
let lastName = prompt("What is your second name?");
let age = +prompt("How old are you?");
let haveChildren = confirm("Do you have children?");
let colorEyes = prompt("What color are your eyes?");

persone.firstName = firstName;
persone.lastName = lastName;
persone.age = age;
persone.haveChildren = haveChildren;
persone.colorEyes = colorEyes;


if (isNaN(age) || age < 15 || age < 100) {
    delete persone.age;
    alert("введено некорректний вік!");
}


for (const key in persone) {
    console.log(`Властивість ${key}: ${persone[key]}`);
}