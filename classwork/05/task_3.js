/*
Створити об'єкт danItStudent,
у якого такі властивості: ім'я, прізвище, кількість зданих домашніх работ.
Запитати у користувача "Що ви хочете дізнатися про студента?"
Якщо користувач ввів "name" або "ім'я" - вивести у консоль ім'я студента.
Якщо користувач ввів "lastName" або "прізвище" - вивести до консолі прізвище студента.
Якщо користувач запровадив "оцінка" або "homeworks" - вивести в консоль кількість зданих робіт.
Якщо ввів щось не з перерахованого - вивести в консоль - "Вибачте таких даних нема"
*/

const danItStudent = {
    firstName: "Mariia",
    lastName: "Koval",
    numberOfSubmittedHomewors: "6"
}

let userQuestion = prompt("Що ви хочете дізнатися про студента?");

if (userQuestion === "name" || userQuestion === "ім'я") {
    console.log(danItStudent.firstName);
} else if (userQuestion === "lastName" || userQuestion === "прізвище"){
    console.log(danItStudent.lastName);
} else if (userQuestion === "homeworks" || userQuestion === "оцінка") {
    console.log(danItStudent.numberOfSubmittedHomewors);
} else {
    console.log("Вибачте таких даних нема");
}