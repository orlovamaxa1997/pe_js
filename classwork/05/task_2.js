/*
Запитати в користувача ім'я, прізвище і чи хоче він їсти.
Перевірити, що користувач щось увів під час введення імені чи прізвища.
Інакше - перепитувати.
Записати отримані дані в об'єкт Person.
В об'єкті створити 2 методи - eat, завдання якого виводити алерт із текстом
"Піду поїм", go, завдання якого виводити алерт із текстом "Тоді я пішов".

Якщо користувач вказав, що хоче їсти, викликати метод eat, інакше - метод go.
*/


let userFirstName;
let userLastName;
let userEat = confirm("Do you want eat?");

while (!userFirstName || !userLastName) {
    userFirstName = prompt("What is your first name?");
    userLastName = prompt("What is your last name?");
}

const persone = {
    firstName: userFirstName,
    lastName: userLastName,
    eat: () => {
        alert("Піду поїм");
    },
    go: () => {
        alert("Тоді я пішов");
    }
}

if (userEat === true) {
    persone.eat;
} else {
    persone.go
}
