// Напишите функцию, которая принимает строку в качестве аргумента
// и преобразует регистр первого символа строки из нижнего регистра в верхний.
function transformUpperCase (str) {
    return str[0].toUpperCase() + str.slice(1).toLowerCase(1);
}

console.log(transformUpperCase("DIMA"));