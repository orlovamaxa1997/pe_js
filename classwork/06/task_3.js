/*
Create 2 objects which will describe HTML-elements.
1 object for div element and second is for p element. 2 objects have the
following properties: className, text, id and method render.
Render method should use document write and show the element with attributes from
object on the page.
*/


const divElement = {
    className: "div",
    text: "div element",
    id: "header",
    render(){
        document.write(`<div class="${this.className}" text="${this.text} id="${this.header}">`)
    }
}

const pElement = {
    className: "class",
    text: "p element",
    id: "text",
    render(){
        document.write(`<p class="${this.className}" text="${this.text} id="${this.header}">`)
    }
}

divElement.render();
pElement.render();