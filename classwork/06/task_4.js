/*
  * Описание задачи: Напишите функцию, которая проверяет, является ли элемент именно простым объектом, а не массивом, null и т.п.
  * Ожидаемый результат: true если это объект, false в противном случае. ({ a: 1 }) => true, ([1, 2, 3]) => false
  * @param element - элемент для проверки
  * @returns {boolean}
*/


function validateElement(obj) {
    return obj === "object" && obj !== null && !Array.isArray(obj);
}

validateElement();