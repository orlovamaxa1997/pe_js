/*

  Написати функцію-фабрику, яка повертає об'єкти користувачів.

  Об'єкт користувача має три властивості:
  - Ім'я;
 - Прізвище;
 - Професія.

 Функція-фабрика в свою чергу має три параметри,
 що відповідають вищеописаним властивостям об'єкта.
 Кожен параметр функції має значення за замовчуванням: null.


*/



function factory (firstName = null, lastName = null, proffession = null) {
    return {
        firstName,
        lastName,
        proffession
    }
}

const user = factory("Maria", "Koval", "HR");