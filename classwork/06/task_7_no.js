// Створити функцію фабрику для html елементів. Вона повинна в якості аргументу приймати об'єкт з параметрами tagName, className, Id, text, style, children. Кожен html елемент повинен мати метод render який виводить цей елемент на сторінку. Створити за допомогою фабрики 4 елемента, вивести всі на сторінку.


function factory(tag, className, id, text, style, children) {
    return {
        tag,
        className,
        id,
        text,
        style,
        children,
        getElement(){
            <${tag}></$>
        },
        render(){
            document.write(`div class="${this.tag}", class = ${this.className}, id = ${this.id}, text = ${this.text}, style = ${this.style}, children = ${this.children}`)

        }
    }
}