/*
Запитати в користувача його ім'я, прізвище, вік.
З цими даними створити об'єкт person
за допомогою функції попереднього завдання,
Доповнивши об'єкт методом sayHi, який виводитиме в консоль
`Привіт, я ${властивість ім'я}, ${властивість прізвище}, мені ${властивість вік}`.
У створеного об'єкта, викликати метод sayHi;
 */

const firstName = prompt("What is your name?");
const lastName = prompt("What is your last name?");
const age = +prompt("What is your age?");

function factory(firstName = null, lastName = null, age = null) {
    return {
        firstName,
        lastName,
        age,
        sayHi() {
            console.log(`Привіт, я ${this.firstName}, ${this.lastName}, мені ${this.age}`);
        }
    }
}

const persone = factory(firstName, lastName, age);
persone.sayHi();