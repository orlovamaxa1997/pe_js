/*
Получить от пользователя 2 числа.
Спрашивать его число заново до тех пор, пока он не введет валидное число.

Проверку на валидность - оформить в функцию, которой мы передаем значание, а функция проверяет значение на null, на пустую строку, на NaN и возвращает true если число валидное, иначе false;

Если 2 числа валидные, вывести в консоль и на экран - результат их сложения
*/

let firstNumber;
let secondNumber;

function validateNumber (value) {
    if (value === null){
        return false;
    }
    else if (value === "") {
        return false;
    }
    else if (Number.isNaN(+value)){
        return false;
    }
    else if (value === 0){
        return false;
    }
    else{
        return true;
    }
}
//
// console.log("number", validateNumber(4));
// console.log("undefined", validateNumber());
// console.log("empty", validateNumber(""));
// console.log("string", validateNumber("text"));
// console.log("null", validateNumber(null));



while (!validateNumber(firstNumber) || !validateNumber(secondNumber)) {
    firstNumber = +prompt("What is first number?", "3");
    secondNumber = +prompt("What is second number?", "2");
}
console.log(
    !validateNumber(firstNumber),
    !validateNumber(secondNumber),
    !validateNumber(firstNumber) && !validateNumber(secondNumber)
)

alert(firstNumber + secondNumber)


if (2 === 0 || [1,2,3].includes(9)) console.log(222)



