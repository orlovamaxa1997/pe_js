/*
Пользователь вводит в модальное окно любое число.
Если пользователь ввёл не число, вывести новое модальное окно с сообщением «Необходимо ввести число!».

В консоль вывести сообщение:
- Если число чётное, вывести в консоль сообщение «Ваше число чётное.»;
- Если число не чётное, вывести в консоль сообщение «Ваше число не чётное.»;
*/

let userInput;

function validateNumber (value) {
    if (value === null){
        return false;
    }
    else if (value === "") {
        return false;
    }
    else if (Number.isNaN(+value)){
        return false;
    }
    else if (value === 0){
        return false;
    }
    else{
        return true;
    }
}

while (!validateNumber(userInput)){
   userInput = +prompt("Введіть число");
}

console.log(`Ваше число ${userInput % 2 ? "не парне" : "парне"}`);


