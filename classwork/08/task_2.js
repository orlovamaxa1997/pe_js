/*
Есть массив брендов автомобилей ["bMw", "Audi", "teSLa", "toYOTA"].
Вам нужно получить новый массив, объектов типа
{
    type: 'car'
    brand: ${элемент массива}
}

Вывести массив в консоль
*/

const autoBrands = ["bMw", "Audi", "teSLa", "toYOTA"];

const autoMappes = autoBrands.map((item) => {
    return{
        type: 'car',
        brand: item
    }
})
console.log(autoMappes);