/*
Создать массив объектов students (в количестве 7 шт).
У каждого студента должно быть имя, фамилия и направление обучения - Full-stask
или Front-end. У каждого студента должен быть метод, sayHi, который возвращает строку
`Привет, я ${имя}, студент Dan, направление ${направление}`.
Перебрать каждый объект и вызвать у каждого объекта метод sayHi;
*/

const student = [
    {
        lastName: "Mike",
        secondName: "Fokus",
        education: "full-stack",
        sayHi(){
            console.log(`Привіт, я ${this.lastName}, студент Dan, направлення ${this.education}`)
        }
    },
    {
        lastName: "Kate",
        secondName: "Mouse",
        education: "front-end",
        sayHi(){
            console.log(`Привіт, я ${this.lastName}, студент Dan, направлення ${this.education}`)
        }
    },
    {
        lastName: "Mary",
        secondName: "Ous",
        education: "front-end",
        sayHi(){
            console.log(`Привіт, я ${this.lastName}, студент Dan, направлення ${this.education}`)
        }
    },
    {
        lastName: "Nike",
        secondName: "Flous",
        education: "full-stack",
        sayHi(){
            console.log(`Привіт, я ${this.lastName}, студент Dan, направлення ${this.education}`)
        }
    }
]

student.forEach(function (student){
    student.sayHi();
})

