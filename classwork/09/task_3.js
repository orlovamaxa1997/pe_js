// Створити функцію, яка буде отримувати масив об'єктів вигляду [
//     {name: 'width', value: 10},
//     {name: 'height', value: 20}
//    ]
// Функція повинна повернути обʼєкт вигляду {width: 10, height: 20}
// Довжина масиву може бути будь якою.


let source = [
    {name: 'width', value: 10,},
    {name: 'height', value: 20},
    {name: 'age', value: 34}
];

const resultSource = source.reduce(
    (acc, item) => ({...acc, [item.name] : item.value}),
    {}
)

console.log(resultSource)
//
//
// const pew = "haha"
//
// const lol = {}
//
// lol[pew] = 22;
//
// lol = {
//     haha: 22
// }

