/*
Завдання

Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm.
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.

Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.

Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel.
Якщо користувач натиснув Ok,показати на екрані повідомлення: Welcome, + ім'я користувача.
Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
...............
Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.

Після введення даних додати перевірку їхньої коректності.
Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).
*/


let userName;
let userAge;


while (!userName){
    userName = prompt("What is your name?", "Kate");
}
while (!(typeof userAge === "number")){
    const userInput = prompt("What is your age?", userAge);

    if (!userInput) userAge = userInput;
    else if (isNaN(+userInput)){
        userAge = userInput;
    } else {
        userAge = +userInput;
    }
}


if (userAge > 22){
    alert(`Welcome ${userName}`);
}
else if (userAge >= 18){
    if (confirm("Are you sure you want to continue?"))
        alert(`Welcome ${userName}`);
    else {
        alert("You are not allowed to visit this website");
    }
}
else {
    alert("You are not allowed to visit this website");
}
