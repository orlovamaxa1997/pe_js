/*
Завдання
Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

    Технічні вимоги:
Отримати за допомогою модального вікна браузера два числа.
Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.

Створити функцію, в яку передати два значення та операцію.

Вивести у консоль результат виконання функції.
    Необов'язкове завдання підвищеної складності
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).
*/

let firstNumber = prompt("What is first number?");
let secondNumber = prompt("What is second number?");
const sign = prompt("Field for +, -, *, /");

function math (firstNumber, secondNumber, sign) {
    switch (sign){
        case "+" :
            return firstNumber + secondNumber;
        case "-" :
            return firstNumber - secondNumber;
        case "*" :
            return firstNumber * secondNumber;
        case "/" :
            return firstNumber / secondNumber;
        default :
            return "Error";
    }
}

function validateNumber (value) {
    if (value === null){
        return false;
    }
    else if (value === "") {
        return false;
    }
    else if (Number.isNaN(+value)){
        return false;
    }
    else return value !== 0;
}

while (!validateNumber(firstNumber) || !validateNumber(secondNumber)) {
    firstNumber = +prompt("What is first number?", firstNumber);
    secondNumber = +prompt("What is second number?", secondNumber);
}

console.log(math(firstNumber, secondNumber, sign));