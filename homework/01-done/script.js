// Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.

const name = "Maria";
let admin = name;

console.log(admin);


// Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.

const num = 5;
const secondsDay = 86400;
const secondNum = num * secondsDay;

console.log(secondNum)


// Запитайте у користувача якесь значення і виведіть його в консоль.

let name = prompt("What is your name?")
console.log(name)