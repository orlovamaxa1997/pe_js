/*
Завдання
Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.

    Технічні вимоги:
    Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:

    При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.

    Створити метод getAge() який повертатиме скільки користувачеві років.
    Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
*/
function createNewUser() {
    return {
        firstName: prompt("What's you name?" , "Maria"),
        lastName: prompt("What's you surname?" , "Orlova"),
        birthday: new Date(prompt("Date of birthday dd.mm.yyyy", "10.09.1997")),
        getAge: function () {
            return this.birthday.getFullYear();
        },
        getPassword : function () {
            return `${this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.getAge()}`
        }
    };
}
const newUser = createNewUser().getPassword();
console.log(newUser);




